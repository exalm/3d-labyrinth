public class Gizmo : Gtk.Widget {
    public Gizmo (string css_name = "widget") {
        Object (css_name: css_name);
    }

    protected override void dispose () {
        var child = get_first_child ();

        while (child != null) {
            var c = child;
            child = child.get_next_sibling ();

            c.unparent ();
        }

        base.dispose ();
    }
}

public class LabyrinthView : Gtk.Widget {
    private const int TILE_SIZE = 64;
    private const float MOVEMENT_SPEED = 2.5f;

    private struct Tile {
        Graphene.Point position;
        Gtk.Widget floor;
        Gtk.Widget ceiling;
        Gtk.Widget?[] walls;
    }

    private Tile?[,] tiles;

    private bool moving_forward;
    private bool moving_backward;
    private bool rotating_left;
    private bool rotating_right;

    private Graphene.Point position;
    private float rotation;

    private ulong timeout_id;

    static construct {
        set_css_name ("labyrinth");
    }

    private bool key_pressed_cb (uint keyval, uint keycode, Gdk.ModifierType state) {
        switch (keyval) {
            case Gdk.Key.Up:
                moving_forward = true;
                break;
            case Gdk.Key.Down:
                moving_backward = true;
                break;
            case Gdk.Key.Left:
                rotating_left = true;
                break;
            case Gdk.Key.Right:
                rotating_right = true;
                break;
            default:
                break;
        }

        return Gdk.EVENT_STOP;
    }

    private void key_released_cb (uint keyval, uint keycode, Gdk.ModifierType state) {
        switch (keyval) {
            case Gdk.Key.Up:
                moving_forward = false;
                break;
            case Gdk.Key.Down:
                moving_backward = false;
                break;
            case Gdk.Key.Left:
                rotating_left = false;
                break;
            case Gdk.Key.Right:
                rotating_right = false;
                break;
            default:
                break;
        }
    }

    private bool tick_cb () {
        if (moving_forward) {
            position = {
                position.x + Math.cosf ((float) (rotation + Math.PI / 2)) * MOVEMENT_SPEED,
                position.y + Math.sinf ((float) (rotation + Math.PI / 2)) * MOVEMENT_SPEED,
            };

            queue_allocate ();
        }

        if (moving_backward) {
            position = {
                position.x - Math.cosf ((float) (rotation + Math.PI / 2)) * MOVEMENT_SPEED,
                position.y - Math.sinf ((float) (rotation + Math.PI / 2)) * MOVEMENT_SPEED,
            };

            queue_allocate ();
        }

        if (rotating_left) {
            rotation -= 0.05f;

            queue_allocate ();
        }

        if (rotating_right) {
            rotation += 0.05f;

            queue_allocate ();
        }

        return Source.CONTINUE;
    }

    construct {
        const char[,] map = {
            {'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X'},
            {'X', ' ', ' ', 'X', 'X', ' ', ' ', 'X'},
            {'X', ' ', 'R', 'R', 'R', 'R', ' ', 'X'},
            {'X', 'X', 'R', 'L', 'L', 'R', 'X', 'X'},
            {'X', 'X', 'R', 'L', 'L', 'R', 'X', 'X'},
            {'X', ' ', 'R', 'R', 'R', 'R', ' ', 'X'},
            {'X', ' ', ' ', 'X', 'X', ' ', ' ', 'X'},
            {'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X'},
        };

        position.init (TILE_SIZE / 2, TILE_SIZE / 2);
        rotation = (float) (-Math.PI / 4);

        tiles = new Tile?[map.length[0], map.length[1]];

        for (int i = 0; i < tiles.length[0]; i++) {
            for (int j = 0; j < tiles.length[1]; j++) {
                if (map[i, j] == ' ')
                    continue;

                Tile t = {};
                t.position.init ((i + 0.5f) * TILE_SIZE, (j + 0.5f) * TILE_SIZE);

                t.floor = new Gizmo ("floor");
                t.floor.can_target = false;
                t.floor.set_parent (this);

                t.ceiling = new Gizmo ("ceiling");
                t.ceiling.can_target = false;
                t.ceiling.set_parent (this);

                t.walls = new Gtk.Widget?[4];

                if (i == 0 || map[i-1, j] == ' ') {
                    t.walls[0] = new Gizmo ("wall");
                    t.walls[0].add_css_class ("top");
                    t.walls[0].can_target = false;
                    t.walls[0].set_parent (this);
                }

                if (i == tiles.length[0] - 1 || map[i+1, j] == ' ') {
                    t.walls[1] = new Gizmo ("wall");
                    t.walls[1].add_css_class ("bottom");
                    t.walls[1].can_target = false;
                    t.walls[1].set_parent (this);
                }

                if (j == 0 || map[i, j-1] == ' ') {
                    t.walls[2] = new Gizmo ("wall");
                    t.walls[2].add_css_class ("left");
                    t.walls[2].can_target = false;
                    t.walls[2].set_parent (this);
                }

                if (j == tiles.length[1] - 1 || map[i, j+1] == ' ') {
                    t.walls[3] = new Gizmo ("wall");
                    t.walls[3].add_css_class ("right");
                    t.walls[3].can_target = false;
                    t.walls[3].set_parent (this);
                }

                if (map[i, j] == 'L') {
                    t.floor.add_css_class ("light");
                    t.ceiling.add_css_class ("light");
                }

                if (map[i, j] == 'R') {
                    foreach (var wall in t.walls)
                        if (wall != null)
                            wall.add_css_class ("red");
                }

                tiles[i, j] = t;
            }
        }

        var controller = new Gtk.EventControllerKey ();
        controller.propagation_phase = Gtk.PropagationPhase.CAPTURE;
        controller.key_pressed.connect (key_pressed_cb);
        controller.key_released.connect (key_released_cb);
        add_controller (controller);

        focusable = true;

        timeout_id = Timeout.add (16, tick_cb);
    }

    private Gsk.Transform get_global_transform (int width, int height) {
        var ret = new Gsk.Transform ();

        ret = ret.translate ({ width / 2, height / 2 });

        float scale = height / 20.0f;
        ret = ret.scale_3d (scale, scale, scale);

        Graphene.Matrix mat = {};

        mat.init_perspective (160, 1f, 1f, 100f);
        mat.inverse (out mat);
        ret = ret.matrix (mat);

        Graphene.Vec3 eye = {}, center = {}, up = {};
        eye.init (-position.x, -position.y, -TILE_SIZE / 2);
        center.init (
            eye.get_x () + (float) Math.cos (rotation + Math.PI / 2),
            eye.get_y () + (float) Math.sin (rotation + Math.PI / 2),
            eye.get_z ()
        );
        up.init (0, 0, -1);

        mat.init_look_at (eye, center, up);
        mat.inverse (out mat);
        ret = ret.matrix (mat);

        return ret;
    }

    protected override void size_allocate (int width, int height, int baseline) {
        var global_transform = get_global_transform (width, height);

        for (int i = 0; i < tiles.length[0]; i++) {
            for (int j = 0; j < tiles.length[1]; j++) {
                var t = tiles[i,j];

                if (t == null)
                    continue;

                Graphene.Vec3 axis = {};

                var transform = new Gsk.Transform ();
                transform = transform.transform (global_transform);
                transform = transform.translate_3d ({ i * TILE_SIZE, j * TILE_SIZE, 0 });
                t.floor.allocate (TILE_SIZE, TILE_SIZE, -1, transform);

                transform = new Gsk.Transform ();
                transform = transform.transform (global_transform);
                transform = transform.translate_3d ({ i * TILE_SIZE, j * TILE_SIZE, TILE_SIZE });
                t.ceiling.allocate (TILE_SIZE, TILE_SIZE, -1, transform);

                if (t.walls[0] != null) {
                    transform = new Gsk.Transform ();
                    transform = transform.transform (global_transform);
                    transform = transform.translate_3d ({ i * TILE_SIZE, (j + 1) * TILE_SIZE, TILE_SIZE });
                    axis.init (-1, 0, 0);
                    transform = transform.rotate_3d (90, axis);
                    axis.init (0, 1, 0);
                    transform = transform.rotate_3d (90, axis);
                    t.walls[0].allocate (TILE_SIZE, TILE_SIZE, -1, transform);
                }

                if (t.walls[1] != null) {
                    transform = new Gsk.Transform ();
                    transform = transform.transform (global_transform);
                    transform = transform.translate_3d ({ (i + 1) * TILE_SIZE, j * TILE_SIZE, TILE_SIZE });
                    axis.init (-1, 0, 0);
                    transform = transform.rotate_3d (90, axis);
                    axis.init (0, -1, 0);
                    transform = transform.rotate_3d (90, axis);
                    t.walls[1].allocate (TILE_SIZE, TILE_SIZE, -1, transform);
                }

                if (t.walls[2] != null) {
                    axis.init (-1, 0, 0);
                    transform = new Gsk.Transform ();
                    transform = transform.transform (global_transform);
                    transform = transform.translate_3d ({ i * TILE_SIZE, j * TILE_SIZE, TILE_SIZE });
                    transform = transform.rotate_3d (90, axis);
                    t.walls[2].allocate (TILE_SIZE, TILE_SIZE, -1, transform);
                }

                if (t.walls[3] != null) {
                    transform = new Gsk.Transform ();
                    transform = transform.transform (global_transform);
                    transform = transform.translate_3d ({ (i + 1) * TILE_SIZE, (j + 1) * TILE_SIZE, TILE_SIZE });
                    axis.init (-1, 0, 0);
                    transform = transform.rotate_3d (90, axis);
                    axis.init (0, -1, 0);
                    transform = transform.rotate_3d (180, axis);
                    t.walls[3].allocate (TILE_SIZE, TILE_SIZE, -1, transform);
                }
            }
        }
    }

    protected override void snapshot (Gtk.Snapshot snapshot) {
        var tiles_to_render = new List<Tile?> ();

        for (int i = 0; i < tiles.length[0]; i++) {
            for (int j = 0; j < tiles.length[1]; j++) {
                var t = tiles[i,j];

                if (t == null)
                    continue;

                tiles_to_render.prepend (t);
            }
        }

        tiles_to_render.sort_with_data ((a, b) => {
            float distance1 = a.position.distance (position, null, null);
            float distance2 = b.position.distance (position, null, null);

            if (distance1 < distance2)
                return 1;

            if (distance1 > distance2)
                return -1;

            return 0;
        });

        foreach (var t in tiles_to_render) {
            for (int k = 0; k < t.walls.length; k++)
                if (t.walls[k] != null)
                    snapshot_child (t.walls[k], snapshot);

            snapshot_child (t.floor, snapshot);
            snapshot_child (t.ceiling, snapshot);
        }
    }

    protected override void dispose () {
        var child = get_first_child ();

        while (child != null) {
            var c = child;
            child = child.get_next_sibling ();

            c.unparent ();
        }

        base.dispose ();
    }
}
